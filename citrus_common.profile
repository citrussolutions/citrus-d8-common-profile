<?php

/**
 * @file
 * Enables modules and site configuration for a Citrus common site installation.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function citrus_common_form_install_settings_form_alter(&$form, FormStateInterface $form_state) {
  // Set default MySQL / MariaDB settings to use the development container.
  $form['settings']['mysql']['advanced_options']['host']['#default_value'] = 'mariadb';
  $form['settings']['mysql']['advanced_options']['port']['#default_value'] = '';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function citrus_common_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Set default value to site mail.
  $form['site_information']['site_mail']['#default_value'] = 'drupal@citrus.fi';

  // Set default values to account information.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'drupal@citrus.fi';

  // Set default values to date and time settings.
  $form['regional_settings']['site_default_country']['#default_value'] = 'FI';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Helsinki';

  // Set default value to update notifications.
  $form['update_notifications']['enable_update_status_module']['#default_value'] = 0;
  $form['update_notifications']['enable_update_status_emails']['#default_value'] = 0;
}

/**
 * Implements hook_preprocess_page().
 */
function citrus_common_preprocess_page(&$variables) {
  $current_path = \Drupal::service('path.current')->getPath();
  if ($current_path == '/admin/structure/block') {
    drupal_set_message(t('Please, do not use this Block Layout page. Use Contexts instead.'), 'error');
  }
}
